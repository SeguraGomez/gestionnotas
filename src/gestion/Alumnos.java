/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Alumnos {
    
    static String[] inicializarLista(int numeroAlumnos) {
        String [] alumnos = new String [numeroAlumnos];
        return alumnos;
    }

    static String[] aniadirAlumnos(int contAlumnoIntroducido, String[] listaAlumnos) {
        System.out.println("Introduce el nombre del alumno");
        Scanner teclado = new Scanner(System.in);
        String nombre = teclado.nextLine();
        listaAlumnos [contAlumnoIntroducido] = nombre;
        return listaAlumnos;
    }

    static void listarAlumnos(String[] listaAlumnos) {
        System.out.println("Este es el listado de alumnos:");

        for (int i = 0; i < listaAlumnos.length; i++) {

            System.out.println(listaAlumnos[i] + " ");

        }

    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
class Notas {
  
    static int[][] inicializarNotas(int numeroAlumnos) {
        int notas[][] =new int[numeroAlumnos][6];
        return notas;
    }

    static int[][] aniadirNota(int[][] listaNotas) {
        int alumno, asignatura;
        int nota;
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce el numero del Alumno");
        alumno=teclado.nextInt();
        System.out.println("Introduce el numero de Asisgnatura");
        asignatura=teclado.nextInt();
        System.out.println("Introduce el numero de Notas");
        nota=teclado.nextInt();
        
        listaNotas[alumno][asignatura] = nota;
        return listaNotas;
    }

    static void mostrarNotasAlumno(int[][] listaNotas) {
        
        for(int i=0; i<listaNotas.length; i++){
            System.out.println("Notas del Alumno " + (i+1));
            for(int j=0; j<6; j++){
                System.out.println("Nota de la Asignatura " + (j+1) + ": " + listaNotas[i][j]);
            }
        }
        
    }
    
}
